import requests
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackContext


def get_main_menu_keyboard():
    keyboard = [
        [
            InlineKeyboardButton("Capitalize text", callback_data=1),
        ],
        [
            InlineKeyboardButton("Send API request", callback_data=2)],
        ]

    return InlineKeyboardMarkup(keyboard)


def echo(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id, text=update.message.text)


def caps(update: Update, context: CallbackContext):
    text_caps = ' '.join(context.args).upper()
    context.bot.send_message(chat_id=update.effective_chat.id, text=text_caps, reply_markup=get_main_menu_keyboard())


def start(update: Update, context: CallbackContext) -> None:
    update.message.reply_text('Please choose:', reply_markup=get_main_menu_keyboard())


def button(update: Update, context: CallbackContext) -> None:

    query = update.callback_query
    query.answer()

    if query.data == str(1):
        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text="to capitalize call /caps text",
                                 reply_markup=get_main_menu_keyboard())

    elif query.data == str(2):
        response = requests.get('http://jsonplaceholder.typicode.com/posts')
        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text="response status: {}".format(response.status_code),
                                 reply_markup=get_main_menu_keyboard())



