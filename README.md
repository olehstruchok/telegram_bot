# Telegram Bot

## Development
* Python 3.7
* this bot use library [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot>) 
* To begin, you'll need a Bot Token. To generate a Bot Token, you have to talk to [@BotFather](https://telegram.me/botfather) and follow a few simple steps 
## Deployment

```bash
pip install virtualenv
virtualenv -p /usr/bin/python3.7 venv
source venv/bin/activate

cp .env.example .env
```

Fill env with your bot token


## Start bot pooling
```bash
python bot.py
```
